# MixPanel

## Overview

The MixPanel is a service analyzing user behavior across sites and apps. The extractor uses the [MixPanel API](https://developer.mixpanel.com/docs/exporting-raw-data) to export raw data about user behavior and allows to analyze data further. All of the data is returned in `json` format, delimited by new line between records. However, not all records consist of same parameter keys, hence the extractor processes the output into name-value pair (NVP) format.

The output is a table of 3 columns, which is **not** loaded incrementally.

#### Note on the exports
Due to vast amount of data, the extractor may run for hours, hence it is recommended to split the data into parts, if a full download is required. You can do so, by correctly specifying `From date` and `To date` parameters.
In case that full download of the data is not required (i.e. extractor is ran every night in the orchestration), it's best to use `Throwback` parameter, which automatically downloads the specified number of full days of data. Specifying `Throwback=-1` downloads 1 last day full of data, i.e. date prior to day; `Throwback=-2` downloads data for 2 full days prior to today, etc.

## Configuration

##### Note on the exports

Due to vast amount of data, the extractor may run for hours, hence it is recommended to split the data into parts, if a full download is required. You can do so, by correctly specifying `From date` and `To date` parameters.
In case that full download of the data is not required (i.e. extractor is ran every night in the orchestration), it's best to use `Throwback` parameter, which automatically downloads the specified number of full days of data. Specifying `Throwback=-1` downloads 1 last day full of data, i.e. date prior to day; `Throwback=-2` downloads data for 2 full days prior to today, etc.

##### Note on timezones

All timestamps returned from the extractor are in the timezone, of your project; **they're not in UTC** timezone.

### Input

The extractor accepts 5 parameters, of which 2 are required and the rest requires to have a combination present. The parameters are:

  - `API Secret` (required) - API secret for the project from which the data should be downloaded. [Click here](https://help.mixpanel.com/hc/en-us/articles/115004490503-Project-Token-API-Key-API-Secret) for information on how to retrieve the API secret.
  - `Residency Server Region` (optional) - Region, where your data are located. Currently, US and EU regions are supported.
  - `From date` (optional) - The date from which, the data should be downloaded. It's required to use `YYYY-MM-DD` format, otherwise, an exception is raised. If left blank, extractor will automatically ignore the parameter and use the value from `Throwback`.
  - `To date` (optional) - The date to which, the data should be downloaded. It's required to use `YYYY-MM-DD` format, otherwise, an exception is raised. If left blank, extractor will automatically ignore the parameter and use the value from `Throwback`.
  - `Throwback` (optional) - Number of the latest full days prior to today, that should be downloaded. The parameter is ignored if both `From date` and `To date` are specified.
  - `Events` (optional) - A list of events, for which the export should be filtered.
  - `Where` (optional) - A string that will be used for the where filter when downloading data.
  - `Timezone` (required) - Timezone, in which your project is located. The parameter is required for `Throwback` parameter to work correctly. Improper definition of `Timezone` may lead to incomplete data. [Click here](https://help.mixpanel.com/hc/en-us/articles/115004547203-Project-Timezone) for more information.
  - `Load Type` (optional) - Choose whether to load table incrementally or via full load.
  - `Raw Data Export` (optional) - If set to `false`, the response from the API will be parsed and stored in storage in a key-value pair table. This is due to inconsistency of the returned JSON for each row. If set to `true`, the raw JSON response will be output to the storage and can be later parsed in e.g. Snowflake, Python transformations.

### Output

If raw data export is disabled, the extractor returns an output table with 3 columns. The output table is in the name-value pair format and may take the following form:

| id 	| parameter 	| value 	|
|----------------------------------	|-------------------------	|--------------------	|
| 149883e1c6000df0f5dc4d521725679b 	| event 	| View Payslip Page 	|
| 149883e1c6000df0f5dc4d521725679b 	| properties__time 	| 1561420800 	|
| 149883e1c6000df0f5dc4d521725679b 	| properties__distinct_id 	| 0051o000009eqc1AAA 	|
| 149883e1c6000df0f5dc4d521725679b 	| properties__$city 	| Evesham 	|

The `id` column is created as an MD5 sum of the **unprocessed row** and is hence unique to each row in the **unprocessed output**. The column can be used to piece back together the desired `parameter`s and their `value`s.

If raw data export is enabled, the extractor returns and output table with 2 columns: `id` and `raw`. The `id` columns is created as mentioned above. The `raw` column contains the **unprocessed output**.

### Developer Notes

Do not use the [MixPanel Utils](https://github.com/mixpanel/mixpanel-utils) library to download data, it downloads all data into memory, which will lead to a memory leak in Keboola.