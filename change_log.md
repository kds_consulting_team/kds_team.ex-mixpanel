**0.2.0**
Rewrote source code to utilize KBCEnvHandler library.
Fixed error on separating json files, now they are read from source file line-by-line without separation.

**0.1.2**
Fixed badly ended json files from the API call.

**0.1.1**
Added printing of wrong rows for debugging purposes.

**0.1.0**
Added relative data extraction. Tweaked configuration schema. Added README, descriptions.

**0.0.6**
Added descriptions.

**0.0.5**
Executables fix #2.

**0.0.4**
Fixed executables.

**0.0.3**
Pipeline fixes.

**0.0.2**
Logging_gelf fixes.

**0.0.1**
The beta version of the component. So far, only __to_date__ and __from_date__ parameters are supported.