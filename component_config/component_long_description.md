# MixPanel

## Overview

The MixPanel is a service analyzing user behavior across sites and apps. The extractor uses the [MixPanel API](https://developer.mixpanel.com/docs/exporting-raw-data) to export raw data about user behavior and allows to analyze data further. All of the data is returned in `json` format, delimited by new line between records. However, not all records consist of same parameter keys, hence the extractor processes the output into name-value pair (NVP) format.

The output is a table of 3 columns, which is **not** loaded incrementally.

#### Note on the exports
Due to vast amount of data, the extractor may run for hours, hence it is recommended to split the data into parts, if a full download is required. You can do so, by correctly specifying `From date` and `To date` parameters.
In case that full download of the data is not required (i.e. extractor is ran every night in the orchestration), it's best to use `Throwback` parameter, which automatically downloads the specified number of full days of data. Specifying `Throwback=-1` downloads 1 last day full of data, i.e. date prior to day; `Throwback=-2` downloads data for 2 full days prior to today, etc.
