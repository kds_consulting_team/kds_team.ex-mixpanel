### Input

The extractor accepts 5 parameters, of which 2 are required and the rest requires to have a combination present. The parameters are:

  - `API Secret` (required) - API secret for the project from which the data should be downloaded. [Click here](https://help.mixpanel.com/hc/en-us/articles/115004490503-Project-Token-API-Key-API-Secret) for information on how to retrieve the API secret.
  - `Residency Server Region` (optional) - Region, where your data are located. Currently, US and EU regions are supported.
  - `From date` (optional) - The date from which, the data should be downloaded. It's required to use `YYYY-MM-DD` format, otherwise, an exception is raised. If left blank, extractor will automatically ignore the parameter and use the value from `Throwback`.
  - `To date` (optional) - The date to which, the data should be downloaded. It's required to use `YYYY-MM-DD` format, otherwise, an exception is raised. If left blank, extractor will automatically ignore the parameter and use the value from `Throwback`.
  - `Throwback` (optional) - Number of the latest full days prior to today, that should be downloaded. The parameter is ignored if both `From date` and `To date` are specified.
  - `Events` (optional) - A list of events, for which the export should be filtered.
  - `Where` (optional) - An expression to filter events by. More info on expression sequence structure can be found [here](https://developer.mixpanel.com/reference/segmentation-expressions)
  - `Timezone` (required) - Timezone, in which your project is located. The parameter is required for `Throwback` parameter to work correctly. Improper definition of `Timezone` may lead to incomplete data. [Click here](https://help.mixpanel.com/hc/en-us/articles/115004547203-Project-Timezone) for more information.
  - `Load Type` (optional) - Choose whether to load table incrementally or via full load.
  - `Raw Data Export` (optional) - If set to `false`, the response from the API will be parsed and stored in storage in a key-value pair table. This is due to inconsistency of the returned JSON for each row. If set to `true`, the raw JSON response will be output to the storage and can be later parsed in e.g. Snowflake, Python transformations.

### Output

If raw data export is disabled, the extractor returns an output table with 3 columns. The output table is in the name-value pair format and may take the following form:

| id 	| parameter 	| value 	|
|----------------------------------	|-------------------------	|--------------------	|
| 149883e1c6000df0f5dc4d521725679b 	| event 	| View Payslip Page 	|
| 149883e1c6000df0f5dc4d521725679b 	| properties__time 	| 1561420800 	|
| 149883e1c6000df0f5dc4d521725679b 	| properties__distinct_id 	| 0051o000009eqc1AAA 	|
| 149883e1c6000df0f5dc4d521725679b 	| properties__$city 	| Evesham 	|

The `id` column is created as an MD5 sum of the **unprocessed row** and is hence unique to each row in the **unprocessed output**. The column can be used to piece back together the desired `parameter`s and their `value`s.

If raw data export is enabled, the extractor returns and output table with 2 columns: `id` and `raw`. The `id` columns is created as mentioned above. The `raw` column contains the **unprocessed output**.
