import logging
import subprocess


class MixPanelClientException(Exception):
    pass


class MixPanelClient():
    def __init__(self, token, export_server):
        self.token = token
        self.export_server = export_server

    def export_events(self, date_from: str, date_to: str, destination: str, events: str = '', where: str = '',
                      header_destination: str = "response-headers.txt") -> None:

        _callCURL = f"""curl -s https://{self.export_server}.mixpanel.com/api/2.0/export/ \
                        -u {self.token}: \
                        -d from_date={date_from} \
                        -d to_date={date_to} \
                        -d event={events} \
                        -d where={where} \
                        -D {header_destination}"""

        logging.debug("cURL call:")
        logging.debug(_callCURL)
        logging.info("Downloading data...")

        with open(destination, 'w') as response:
            subprocess.call(_callCURL.split(), stdout=response)

        with open(header_destination, 'r') as response_headers:

            headers = [h.strip() for h in response_headers.readlines()]

            if '200' not in headers[0]:
                with open(destination, 'r') as response:
                    file = [f.strip() for f in response.readlines()]
                err = headers[0]
                raise MixPanelClientException(
                    f"There was an issue downloading the data. The response received was {err} . Message : {str(file)}")

            else:
                logging.info("Download was successful.")
