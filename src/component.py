import logging
import datetime
import pytz
import urllib
import json
import csv
import tempfile
from hashlib import md5
from typing import Tuple, List, Dict, Literal
from keboola.component.base import ComponentBase
from keboola.component.exceptions import UserException
from keboola.component.dao import TableDefinition
from client import MixPanelClient, MixPanelClientException

KEY_API_SECRET = '#api_secret'
KEY_FROM_DATE = 'from_date'
KEY_TO_DATE = 'to_date'
KEY_TIMEZONE = 'timezone'
KEY_THROWBACK = 'throwback'
KEY_INCREMENTAL = 'incremental'
KEY_SERVER = 'residency_server'
KEY_RAW_EXTRACT = 'raw_extract'
KEY_EVENTS = 'events'
KEY_WHERE = 'where'
KEY_DEBUG = 'debug'

OUTPUT_FIELDS = ['id', 'parameter', 'value']
OUTPUT_PK = ['id', 'parameter']

OUTPUT_FIELDS_RAW = ['id', 'raw']
OUTPUT_PK_RAW = ['id']

REQUIRED_PARAMETERS = [KEY_API_SECRET, KEY_TIMEZONE, [KEY_THROWBACK, [KEY_TO_DATE, KEY_FROM_DATE]]]
REQUIRED_IMAGE_PARS = []


class Component(ComponentBase):
    def __init__(self) -> None:
        super().__init__(required_parameters=REQUIRED_PARAMETERS, required_image_parameters=REQUIRED_IMAGE_PARS)

    def run(self) -> None:
        params = self.configuration.parameters
        api_token = params.get(KEY_API_SECRET)

        if params.get(KEY_DEBUG):
            logging.getLogger().setLevel(logging.DEBUG)

        server = params.get(KEY_SERVER, 'US')
        export_server = self.get_export_server_url(server)

        date_from = params.get(KEY_FROM_DATE)
        date_to = params.get(KEY_TO_DATE)
        timezone = params.get(KEY_TIMEZONE)
        throwback = params.get(KEY_THROWBACK)
        requests_date_from, requests_date_to = self.determine_date(date_from, date_to, timezone, throwback)

        events = params.get(KEY_EVENTS)
        encoded_events = self.encode_events(events)
        where = params.get(KEY_WHERE, "")
        encoded_where = urllib.parse.quote_plus(where)

        client = MixPanelClient(api_token, export_server=export_server)
        result_tmp_file = tempfile.NamedTemporaryFile()
        result_headers_tmp_file = tempfile.NamedTemporaryFile()
        try:
            client.export_events(date_from=requests_date_from, date_to=requests_date_to,
                                 destination=result_tmp_file.name, events=encoded_events, where=encoded_where,
                                 header_destination=result_headers_tmp_file.name)
        except MixPanelClientException as mixpanel_exception:
            raise UserException("Failed to process query, make sure the dates, events, and where filters are all set "
                                "correctly") from mixpanel_exception

        incremental = params.get(KEY_INCREMENTAL, True)
        output_table = self.create_out_table_definition("output.csv", incremental=incremental)
        raw_extract = params.get(KEY_RAW_EXTRACT)

        if raw_extract:
            output_table.primary_key = OUTPUT_PK_RAW
            output_table.columns = OUTPUT_FIELDS_RAW
        else:
            output_table.primary_key = OUTPUT_PK
            output_table.columns = OUTPUT_FIELDS

        self.process_output(result_tmp_file.name, output_table, raw_extract)
        self.write_manifest(output_table)

    @staticmethod
    def determine_date(date_from: str, date_to: str, timezone: str, throwback: int) -> Tuple[str, str]:
        if not date_from or not date_to:
            logging.info("Relative data extraction will be used.")
            dt_utc = datetime.datetime.utcnow().replace(tzinfo=pytz.utc)
            dt_local = dt_utc.astimezone(pytz.timezone(timezone)) - datetime.timedelta(days=1)
            requests_date_to = dt_local.strftime("%Y-%m-%d")
            requests_date_from = (dt_local + datetime.timedelta(days=int(throwback) + 1)).strftime("%Y-%m-%d")
            logging.info(f"Data will be exported from {requests_date_from} to {requests_date_to}.")
        else:
            requests_date_from = date_from
            requests_date_to = date_to
            logging.info(f"Using absolute date export from {requests_date_from} to {requests_date_to}.")
        return requests_date_from, requests_date_to

    @staticmethod
    def encode_events(events: List) -> str:
        if not events:
            return ''
        else:
            return urllib.parse.quote_plus(json.dumps([ev for ev in events if ev.strip() != '']))

    @staticmethod
    def get_export_server_url(server: str) -> Literal['data', 'data-eu']:
        if server == 'US':
            return 'data'
        elif server == 'EU':
            return 'data-eu'
        else:
            raise UserException(f"Unsupported residency server selected: {server}.")

    def process_output(self, input_table: str, output_table: TableDefinition, raw: bool) -> None:
        writer = csv.writer(open(output_table.full_path, 'w'), quotechar='"', quoting=csv.QUOTE_ALL)

        logging.info("Processing output...")
        i = 0

        with open(input_table, 'r') as raw_data:
            for line in raw_data:
                i += 1

                if i % 200000 == 0:
                    logging.info("Processed %s rows so far..." % i)

                try:
                    _line_js = json.loads(line)
                    _line_id = md5(line.encode()).hexdigest()
                except ValueError as e:
                    raise UserException("Error occured while processing the file.") from e
                if raw is True:
                    writer.writerow([_line_id, json.dumps(line)])
                else:
                    _line_flattened = self._json_flatten(_line_js)
                    for param in _line_flattened:
                        writer.writerow([_line_id, param, _line_flattened[param]])
        logging.info("Processed %s rows in total." % i)

    def _json_flatten(self, raw_json: Dict, delimiter: str = '__') -> Dict:

        result_dict = {}
        for key in raw_json:
            if isinstance(raw_json[key], dict):
                get = self._json_flatten(raw_json[key], delimiter)
                for nested_key in get:
                    result_dict[key.replace(' ', '_') + delimiter + nested_key.replace(' ', '_')] = get[nested_key]
            else:
                result_dict[key.replace(' ', '_')] = raw_json[key]

        return result_dict


if __name__ == "__main__":
    try:
        comp = Component()
        comp.run()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
